import sys
import os
import re
from shutil import copy2
from urllib.request import urlopen


HOST_REGEX = r"^(\d{1,3}(?:\.\d{1,3}){3})\s+(.+)$"
DEFAULT_HOST_URLS = [
    'http://adaway.org/hosts.txt',
    'https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext',
]


def backup(path):
    """ Create a backup file for host """
    backup_file = os.path.join(
        os.path.abspath(os.path.dirname(path)), 'hosts.bak')
    if not os.path.exists(backup_file):
        if os.path.exists(path):
            copy2(path, backup_file)
        else:
            error("FileExistsError: Can't create backup file, "
                  "'hosts' file not found.")


def restore(path):
    """ Revert all changes from .bak file """
    backup_file = os.path.join(
        os.path.abspath(os.path.dirname(path)), 'hosts.bak')

    if os.path.exists(backup_file):
        copy2(backup_file, path)
    else:
        error("FileExistsError: Can't find 'hosts.bak' file.")


def merge(hosts):
    domains = []
    for row in hosts.splitlines():
        match = re.match(HOST_REGEX, row)
        if match:
            domains.append(match.group(2))
    domains = list(set(domains))
    return domains


def localhosts(paths):
    hosts = ''
    for path in paths:
        with open(path, 'rb') as host_file:
            r = host_file.read().decode()
            hosts += r + '\n'
    return hosts


def download(urls):
    hosts = ''
    for url in urls:
        r = urlopen(url).read().decode()
        hosts += r + '\n'
    return hosts


def stringify(ip, domain):
    return f"{ip}\t{domain}\n"


def generate(target, domains, path):
    sorted_domains = sorted(domains)

    with open(path, 'w') as host_file:
        for domain in sorted_domains:
            entry = stringify(target, domain)
            host_file.write(entry)


def defaultpath():
    if sys.platform == 'linux':
        return "/etc/hosts"
    else:
        return "C:\Windows\System32\drivers\etc\hosts"


def root_or_admin():
    """ Crossplatform way to check admin/root rights """
    if sys.platform == 'linux':
        return not os.getuid()
    elif sys.platform == 'win32':
        from ctypes import LibraryLoader, WinDLL
        IsUserAnAdmin = WinDLL('Shell32').IsUserAnAdmin
        return IsUserAnAdmin()
    else:
        return False


def error(msg):
    """ Show error message """
    sys.stderr.flush()
    sys.stderr.write(f"{msg} \n")


def info(msg):
    """ Show info message """
    sys.stdout.flush()
    sys.stdout.write(f"{msg} \n")

