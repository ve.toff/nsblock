#!/usr/bin/python
import sys
from argparse import ArgumentParser
from nsblock import *

def get_parser():
    parser = ArgumentParser(description="NSBlock - simple cross-platform"
                            "python script for generating host files"
                            "and blocking ads, trackers and other nasty things.")
    group = parser.add_mutually_exclusive_group()

    group.add_argument('-f', nargs='+', metavar='file',
                       help="take rules from local (host) files.")
    group.add_argument('-r', action='store_true',
                       help="revert all changes.")
    path = defaultpath()
    parser.add_argument('-o', default=path, metavar='path',
                        help=f"path to the host file. (default: {path}")
    parser.add_argument('-t', default='127.0.0.1', metavar='addr',
                        help="the address to which dns will be resolved (defalut: 127.0.0.1).")
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    if not root_or_admin():
        error("PermissionError: Administrator or root rights required!")
        return 1
    else:
        if args.r:
            restore(args.o)
        else:
            backup(args.o)
            if args.f:
                domains = merge(localhosts(args.f))
            else:
                domains = merge(download(DEFAULT_HOST_URLS))

            generate(args.t, domains, args.o)

            info(f"Wrote new changes to: {args.o}")
            info("If there are any errors or you want to revert changes"
                 ", use the -r argument")
    return 0


if __name__ == '__main__':
    sys.exit(main())
